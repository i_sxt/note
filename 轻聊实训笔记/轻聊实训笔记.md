# 第一章 实训主题介绍

## 学习目标：

1. 了解本次实训主题
2. 对本次项目的功能模块有一个全局的把控
3. 给自己注入信心

### 一、实训涉及的技术点

- 前端：
    1. MUI框架的基础使用
    2. H5+的基础使用
    3. AJAX的使用与后端对接的规范
    4. RestFul Api的概念及使用
    5. Websocket协议


- 后端：

  ​	1.javaEE规范

  ​	2.Git代码管理工具（协同开发）

  ​	3.Swagger2接口管理工具

  ​	4.SpringBoot企业级应用开发框架

  ​	5.netty通信技术、WS通信协议

  ​	6.Mybatis ORM数据库框架

  ​	7.Mysql数据库

  ​	8.MAVEN依赖管理和编译打包工具

####  二、课程计划

| 时间 |  类别  |                           内容                           |      备注      |
| :--: | :----: | :------------------------------------------------------: | :------------: |
|  1   | 前后端 |   项目介绍、实训安排、环境搭建、小班分组（一组3-4人）    |                |
|  2   |  前端  |    H5+文档、MUI框架、HUbilderX项目的创建、调试与打包     |                |
|  3   |  前端  |             登录注册页面、首页框架、我的主页             |                |
|  4   |  后端  |         登录注册接口、个人信息的RestFul Api完成          |                |
|  5   |  前端  |                 发现页面及其子页面的完成                 |                |
|  6   |  后端  | 搜索、添加、删除好友逻辑、二维码的生成接口、好友列表查询 |                |
|  7   |  前端  |                 二维码页面、通讯录的渲染                 |                |
|  8   |  后端  |                        聊天逻辑1                         |                |
|  9   |  后端  |                        聊天逻辑2                         |                |
|  10  |  前端  | 聊天页面完成、<u>WebSocket的搭建实现与后端消息的互通</u> |                |
|  11  |  前端  |           聊天功能的实现、聊天快照的提取与保存           | ***时间较长*** |
|  12  | 前后端 |             项目打包部署、项目答疑、项目答辩             |                |



#### 三、实训主题介绍

##### 3.1 聊聊身边的聊天软件

###### 3.1.1 QQ

腾讯QQ，8亿人在用的即时通讯软件，你不仅可以在各类通讯终端上通过QQ聊天交友，还支持在线视频通话、点对点断点续传文件、共享文件、网络硬盘、自定义面板、QQ邮箱等多种功能，并可与多种通讯终端相连。QQ有强大的社交属性，QQ的一个里程碑就是QQ空间，这也是QQ能够在早期爆发的一个重要因素之一；有丰富的拓展性功能，不仅仅与腾讯的各大游戏、服务相结合，成为了很多软件的必要入口之一，还有可以授权第三方对象使用QQ号登录注册，积累了广大用户；非常高的完成度，20年的积累，让QQ非常迎合人们的使用习惯，完成度绝对是其他新兴聊天应用锁无法比拟的；始终保持强大的创新能力，后期关系链的不断循环加强和持续不断的创新让QQ能够经久不衰，比如各式各样的红包、坦白说等等。

###### 3.1.2 微信

微信是一款跨平台的通讯工具。支持单人、多人参与。通过手机网络发送语音、图片、视频和文字。其实微信做的事情和QQ有很多类似之处，但是微信就没有QQ那么多的功能。并且聊天相关的很多功能和QQ也有巨大的差异。但整体比QQ简介很多，并且有一个重量级的功能：微信支付，这使很多人都离不开微信。并且微信拥有众多的小程序，给开发者更多的可能，这也让微信拥有强大的竞争力，和QQ一样也是众多平台的入口之一。

###### 3.1.3 米聊

了解下来，其实大家都不难发现，即时聊天app大部分在开始的时候都是类似的。雷军凭借米聊曾经在2010年底火了一把，并迅速引来了一批用户。但腾讯也不是盖的，紧跟其后发布了微信，仅仅比米聊晚1个月。凭着QQ导入用户，微信迅速成长起来，没过多久就超越米聊。在社交领域方面小米也深知不是腾讯，所以将精力放在发展手机行业上。

##### 3.2轻聊软件介绍

我们这一次开发，仿照微信的ui做设计，实现微信的即时聊天最基本功能。包括用户的注册登录，头像上传修改、设置用户名、修改密码、聊天通讯录、聊天列表实时聊天等基本功能。难度适宜，代码整体不难，业务逻辑是项目的关键。



##### 3.3软件的业务逻辑介绍

<img src="/image/业务逻辑图.png" style="zoom:200%;" />



# 第二章 实训环境准备

## 学习目标：

1. 学会Java环境安装
2. 学会Mysql安装和简易操作
3. 学会Git的安装及简单实用，体会团队开发的协作和代码版本的统一管理
4. 学会Idea的安装及日常使用
5. 学会MAVEN环境安装，了解“依赖”的概念，掌握MAVEN的打包方式
6. 学会HubilderX的安装及日常使用，同时打通电脑到手机的USB调试过程
7. 

## 一、Java环境安装

**window****系统安装****java**

**下载****JDK**

首先我们需要下载java开发工具包JDK，下载地址：http://www.oracle.com/technetwork/java/javase/downloads/index.html，点击如下下载按钮：

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg)

在下载页面中你需要选择接受许可，并根据自己的系统选择对应的版本，本文以 Window 64位系统为例：

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image004.jpg)

下载后JDK的安装根据提示进行，还有安装JDK的时候也会安装JRE，一并安装就可以了。 

安装JDK，安装过程中可以自定义安装目录等信息，例如我们选择安装目录为 **C:\Program Files (x86)\Java\jdk1.8.0_91**。 

**配置环境变量**

1.安装完成后，右击"我的电脑"，点击"属性"，选择"高级系统设置"；

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image006.jpg)

2.选择"高级"选项卡，点击"环境变量"；

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image007.png)

然后就会出现如下图所示的画面：

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image008.png)

在 "系统变量" 中设置 3 项属性，JAVA_HOME、PATH、CLASSPATH(大小写无所谓),若已存在则点击"编辑"，不存在则点击"新建"。

***注意：\****如果使用* *1.5* *以上版本的* *JDK**，不用设置* *CLASSPATH* *环境变量，也可以正常编译和运行* *Java* *程序。*

变量设置参数如下：

·     变量名：**JAVA_HOME** 

·     变量值：**C:\Program Files (x86)\Java\jdk1.8.0_91**     // 要根据自己的实际路径配置

·     变量名：**CLASSPATH** 

·     变量值：**.;%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar;**     //记得前面有个"."

·     变量名：**Path**

·     变量值：**%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;**

**JAVA_HOME** **设置**

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image009.png)

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image010.png)

**PATH****设置**

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image011.png)

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image012.png)

***注意：\****在* *Windows10* *中，**Path* *变量里是分条显示的，我们需要将* ***%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;*** *分开添加，否则无法识别：*

%JAVA_HOME%\bin;

%JAVA_HOME%\jre\bin;

*![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image013.png)*

*更多内容可参考：**[Windows 10 配置Java 环境变量](https://www.runoob.com/w3cnote/windows10-java-setup.html)*

**CLASSPATH** **设置**

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image014.png)

这是 Java 的环境配置，配置完成后，你可以启动 Eclipse 来编写代码，它会自动完成java环境的配置。

**测试****JDK****是否安装成功**

1、"开始"->"运行"，键入"cmd"；

2、键入命令: **java -version**、**java**、**javac** 几个命令，出现以下信息，说明环境变量配置成功；

![img](file:///C:/Users/sxt/AppData/Local/Temp/msohtmlclip1/01/clip_image016.jpg)

 





## 二、Mysql环境安装

* 配置程序兼容性修改

mysql默认安装路径：<u>C:\Program Files\MySQL\MySQL Server 5.5</u>



1.进入bin文件夹，找到MySQLInstanceConfig.exe

2.右键 ， 属性  ，tab：兼容性 ，改为 windows xp模式，再运行配置程序

![image-20191103154911740](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103154911740.png)

![image-20191103154931331](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103154931331.png)





* 远程数据库连接信息：

  ip：47.98.244.28

  port：3366

  username：root

  password：123





## 三、Git环境安装

* git的简易操作

![image-20191103170906533](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103170906533.png)





## 四、Idea的安装和配置



## 五、MAVEN的配置



* 环境变量配置

  

* maven仓库之间的关系

![image-20191103195812454](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103195812454.png)

* maven本地仓库配置

~~~ xml
<!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
  <localRepository>/path/to/local/repo</localRepository>
  -->
  <localRepository>E:\java\maven\Repository</localRepository>
~~~



* maven中央仓库设置

~~~ xml
<mirrors>
    <!-- mirror
     | Specifies a repository mirror site to use instead of a given repository. The repository that
     | this mirror serves has an ID that matches the mirrorOf element of this mirror. IDs are used
     | for inheritance and direct lookup purposes, and must be unique across the set of mirrors.
     |
    <mirror>
      <id>mirrorId</id>
      <mirrorOf>repositoryId</mirrorOf>
      <name>Human Readable Name for this Mirror.</name>
      <url>http://my.repository.com/repo/path</url>
    </mirror>
     -->

	 <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>       
    </mirror> 

  </mirrors>
~~~

* maven完整的配置文件

~~~ xml
<?xml version="1.0" encoding="UTF-8"?>

<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<!--
 | This is the configuration file for Maven. It can be specified at two levels:
 |
 |  1. User Level. This settings.xml file provides configuration for a single user,
 |                 and is normally provided in ${user.home}/.m2/settings.xml.
 |
 |                 NOTE: This location can be overridden with the CLI option:
 |
 |                 -s /path/to/user/settings.xml
 |
 |  2. Global Level. This settings.xml file provides configuration for all Maven
 |                 users on a machine (assuming they're all using the same Maven
 |                 installation). It's normally provided in
 |                 ${maven.conf}/settings.xml.
 |
 |                 NOTE: This location can be overridden with the CLI option:
 |
 |                 -gs /path/to/global/settings.xml
 |
 | The sections in this sample file are intended to give you a running start at
 | getting the most out of your Maven installation. Where appropriate, the default
 | values (values used when the setting is not specified) are provided.
 |
 |-->
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <!-- localRepository
   | The path to the local repository maven will use to store artifacts.
   |
   | Default: ${user.home}/.m2/repository
  <localRepository>/path/to/local/repo</localRepository>
  -->
  <localRepository>E:\java\maven\Repository</localRepository>

  <!-- interactiveMode
   | This will determine whether maven prompts you when it needs input. If set to false,
   | maven will use a sensible default value, perhaps based on some other setting, for
   | the parameter in question.
   |
   | Default: true
  <interactiveMode>true</interactiveMode>
  -->

  <!-- offline
   | Determines whether maven should attempt to connect to the network when executing a build.
   | This will have an effect on artifact downloads, artifact deployment, and others.
   |
   | Default: false
  <offline>false</offline>
  -->

  <!-- pluginGroups
   | This is a list of additional group identifiers that will be searched when resolving plugins by their prefix, i.e.
   | when invoking a command line like "mvn prefix:goal". Maven will automatically add the group identifiers
   | "org.apache.maven.plugins" and "org.codehaus.mojo" if these are not already contained in the list.
   |-->
  <pluginGroups>
    <!-- pluginGroup
     | Specifies a further group identifier to use for plugin lookup.
    <pluginGroup>com.your.plugins</pluginGroup>
    -->
  </pluginGroups>

  <!-- proxies
   | This is a list of proxies which can be used on this machine to connect to the network.
   | Unless otherwise specified (by system property or command-line switch), the first proxy
   | specification in this list marked as active will be used.
   |-->
  <proxies>
    <!-- proxy
     | Specification for one proxy, to be used in connecting to the network.
     |
    <proxy>
      <id>optional</id>
      <active>true</active>
      <protocol>http</protocol>
      <username>proxyuser</username>
      <password>proxypass</password>
      <host>proxy.host.net</host>
      <port>80</port>
      <nonProxyHosts>local.net|some.host.com</nonProxyHosts>
    </proxy>
    -->
  </proxies>

  <!-- servers
   | This is a list of authentication profiles, keyed by the server-id used within the system.
   | Authentication profiles can be used whenever maven must make a connection to a remote server.
   |-->
  <servers>
    <!-- server
     | Specifies the authentication information to use when connecting to a particular server, identified by
     | a unique name within the system (referred to by the 'id' attribute below).
     |
     | NOTE: You should either specify username/password OR privateKey/passphrase, since these pairings are
     |       used together.
     |
    <server>
      <id>deploymentRepo</id>
      <username>repouser</username>
      <password>repopwd</password>
    </server>
    -->

    <!-- Another sample, using keys to authenticate.
    <server>
      <id>siteServer</id>
      <privateKey>/path/to/private/key</privateKey>
      <passphrase>optional; leave empty if not used.</passphrase>
    </server>
    -->
  </servers>

  <!-- mirrors
   | This is a list of mirrors to be used in downloading artifacts from remote repositories.
   |
   | It works like this: a POM may declare a repository to use in resolving certain artifacts.
   | However, this repository may have problems with heavy traffic at times, so people have mirrored
   | it to several places.
   |
   | That repository definition will have a unique id, so we can create a mirror reference for that
   | repository, to be used as an alternate download site. The mirror site will be the preferred
   | server for that repository.
   |-->
  <mirrors>
    <!-- mirror
     | Specifies a repository mirror site to use instead of a given repository. The repository that
     | this mirror serves has an ID that matches the mirrorOf element of this mirror. IDs are used
     | for inheritance and direct lookup purposes, and must be unique across the set of mirrors.
     |
    <mirror>
      <id>mirrorId</id>
      <mirrorOf>repositoryId</mirrorOf>
      <name>Human Readable Name for this Mirror.</name>
      <url>http://my.repository.com/repo/path</url>
    </mirror>
     -->

	 <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>       
    </mirror> 

  </mirrors>

  <!-- profiles
   | This is a list of profiles which can be activated in a variety of ways, and which can modify
   | the build process. Profiles provided in the settings.xml are intended to provide local machine-
   | specific paths and repository locations which allow the build to work in the local environment.
   |
   | For example, if you have an integration testing plugin - like cactus - that needs to know where
   | your Tomcat instance is installed, you can provide a variable here such that the variable is
   | dereferenced during the build process to configure the cactus plugin.
   |
   | As noted above, profiles can be activated in a variety of ways. One way - the activeProfiles
   | section of this document (settings.xml) - will be discussed later. Another way essentially
   | relies on the detection of a system property, either matching a particular value for the property,
   | or merely testing its existence. Profiles can also be activated by JDK version prefix, where a
   | value of '1.4' might activate a profile when the build is executed on a JDK version of '1.4.2_07'.
   | Finally, the list of active profiles can be specified directly from the command line.
   |
   | NOTE: For profiles defined in the settings.xml, you are restricted to specifying only artifact
   |       repositories, plugin repositories, and free-form properties to be used as configuration
   |       variables for plugins in the POM.
   |
   |-->
  <profiles>
    <!-- profile
     | Specifies a set of introductions to the build process, to be activated using one or more of the
     | mechanisms described above. For inheritance purposes, and to activate profiles via <activatedProfiles/>
     | or the command line, profiles have to have an ID that is unique.
     |
     | An encouraged best practice for profile identification is to use a consistent naming convention
     | for profiles, such as 'env-dev', 'env-test', 'env-production', 'user-jdcasey', 'user-brett', etc.
     | This will make it more intuitive to understand what the set of introduced profiles is attempting
     | to accomplish, particularly when you only have a list of profile id's for debug.
     |
     | This profile example uses the JDK version to trigger activation, and provides a JDK-specific repo.
    <profile>
      <id>jdk-1.4</id>

      <activation>
        <jdk>1.4</jdk>
      </activation>

      <repositories>
        <repository>
          <id>jdk14</id>
          <name>Repository for JDK 1.4 builds</name>
          <url>http://www.myhost.com/maven/jdk14</url>
          <layout>default</layout>
          <snapshotPolicy>always</snapshotPolicy>
        </repository>
      </repositories>
    </profile>
    -->

    <!--
     | Here is another profile, activated by the system property 'target-env' with a value of 'dev',
     | which provides a specific path to the Tomcat instance. To use this, your plugin configuration
     | might hypothetically look like:
     |
     | ...
     | <plugin>
     |   <groupId>org.myco.myplugins</groupId>
     |   <artifactId>myplugin</artifactId>
     |
     |   <configuration>
     |     <tomcatLocation>${tomcatPath}</tomcatLocation>
     |   </configuration>
     | </plugin>
     | ...
     |
     | NOTE: If you just wanted to inject this configuration whenever someone set 'target-env' to
     |       anything, you could just leave off the <value/> inside the activation-property.
     |
    <profile>
      <id>env-dev</id>

      <activation>
        <property>
          <name>target-env</name>
          <value>dev</value>
        </property>
      </activation>

      <properties>
        <tomcatPath>/path/to/tomcat/instance</tomcatPath>
      </properties>
    </profile>
    -->
  </profiles>

  <!-- activeProfiles
   | List of profiles that are active for all builds.
   |
  <activeProfiles>
    <activeProfile>alwaysActiveProfile</activeProfile>
    <activeProfile>anotherAlwaysActiveProfile</activeProfile>
  </activeProfiles>
  -->
</settings>

~~~



* idea 本地maven的配置

![image-20191103202337040](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103202337040.png)



## 六、HubilderX的安装

















# 第三章 Spring介绍及SpringBoot的引入

## 学习目标：

1. 能够理解Spring的优缺点
2. 能够理解SpringBoot的特点
3. 能够理解SpringBoot的核心功能
4. 能够搭建SpringBoot的环境
5. 能够完成application.properties配置文件的配置
6. 能够完成application.yml配置文件的配置
7. 能够使用SpringBoot集成Mybatis
8. 能够使用SpringBoot集成Junit

# 一、SpringBoot简介

## 1.1  原有Spring优缺点分析

### 1.1.1 Spring的优点分析

Spring是Java企业版（Java Enterprise Edition，JEE，也称J2EE）的轻量级代替品。无需开发重量级的Enterprise JavaBean（EJB），Spring为企业级Java开发提供了一种相对简单的方法，通过依赖注入和面向切面编程，用简单的Java对象（Plain Old Java Object，POJO）实现了EJB的功能。

### 1.1.2 Spring的缺点分析

虽然Spring的组件代码是轻量级的，但它的配置却是重量级的。一开始，Spring用XML配置，而且是很多XML配置。Spring 2.5引入了基于注解的组件扫描，这消除了大量针对应用程序自身组件的显式XML配置。Spring 3.0引入了基于Java的配置，这是一种类型安全的可重构配置方式，可以代替XML。

所有这些配置都代表了开发时的损耗。因为在思考Spring特性配置和解决业务问题之间需要进行思维切换，所以编写配置挤占了编写应用程序逻辑的时间。和所有框架一样，Spring实用，但与此同时它要求的回报也不少。

除此之外，项目的依赖管理也是一件耗时耗力的事情。在环境搭建时，需要分析要导入哪些库的坐标，而且还需要分析导入与之有依赖关系的其他库的坐标，一旦选错了依赖的版本，随之而来的不兼容问题就会严重阻碍项目的开发进度。

## 1.2 SpringBoot的概述

### 1.2.1 SpringBoot解决上述Spring的缺点

SpringBoot对上述Spring的缺点进行的改善和优化，基于约定优于配置的思想，可以让开发人员不必在配置与逻辑业务之间进行思维的切换，全身心的投入到逻辑业务的代码编写中，从而大大提高了开发的效率，一定程度上缩短了项目周期。

### 1.2.2 SpringBoot的特点

- 为基于Spring的开发提供更快的入门体验
- 开箱即用，没有代码生成，也无需XML配置。同时也可以修改默认值来满足特定的需求
- 提供了一些大型项目中常见的非功能性特性，如嵌入式服务器、安全、指标，健康检测、外部配置等
- SpringBoot不是对Spring功能上的增强，而是提供了一种快速使用Spring的方式

### 1.2.3 SpringBoot的核心功能

- 起步依赖

  起步依赖本质上是一个Maven项目对象模型（Project Object Model，POM），定义了对其他库的传递依赖，这些东西加在一起即支持某项功能。

  简单的说，起步依赖就是将具备某种功能的坐标打包到一起，并提供一些默认的功能。

- 自动配置

  Spring Boot的自动配置是一个运行时（更准确地说，是应用程序启动时）的过程，考虑了众多因素，才决定Spring配置应该用哪个，不该用哪个。该过程是Spring自动完成的。

# 二、SpringBoot快速入门

## 2.1 代码实现

### 2.1.1 创建Maven工程

![image-20191103202530714](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103202530714.png)

![image-20191103212637994](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103212637994.png)

![image-20191103212659721](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103212659721.png)

![image-20191103202457726](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191103202457726.png)







### 2.1.2 添加SpringBoot的起步依赖

SpringBoot要求，项目要继承SpringBoot的起步依赖spring-boot-starter-parent

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.0.1.RELEASE</version>
</parent>
```

SpringBoot要集成SpringMVC进行Controller的开发，所以项目要导入web的启动依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
</dependencies>
```



### 2.1.3 编写SpringBoot引导类

要通过SpringBoot提供的引导类起步SpringBoot才可以进行访问

```java
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author sxt
 * @create 2019-11-03 20:54
 */
@SpringBootApplication
public class LightChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(LightChatApplication.class,args);
    }

}                                                        
```

### 2.1.4 编写Controller

在引导类MySpringBootApplication同级包或者子级包中创建QuickStartController

```java
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author sxt
 * @create 2019-11-03 21:21
 */

@RestController
public class TestController {

    @RequestMapping("/test")
    public String test(){
        return "hello , success!";
    }
}

```

### 2.1.5 测试

执行SpringBoot起步类的主方法，控制台打印日志如下：

```
	 .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.8.RELEASE)

2019-11-03 21:22:56.123  INFO 19636 --- [           main] com.lightChat.LightChatApplication       : Starting LightChatApplication on DESKTOP-15Q2QRT with PID 19636 (C:\Users\sxt\Desktop\lightChat\target\classes started by sxt in C:\Users\sxt\Desktop\lightChat)
2019-11-03 21:22:56.129  INFO 19636 --- [           main] com.lightChat.LightChatApplication       : No active profile set, falling back to default profiles: default
2019-11-03 21:22:58.535  INFO 19636 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2019-11-03 21:22:58.654  INFO 19636 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2019-11-03 21:22:58.654  INFO 19636 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.24]
2019-11-03 21:22:58.896  INFO 19636 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2019-11-03 21:22:58.896  INFO 19636 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 2691 ms
2019-11-03 21:22:59.162  INFO 19636 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2019-11-03 21:22:59.399  INFO 19636 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2019-11-03 21:22:59.403  INFO 19636 --- [           main] com.lightChat.LightChatApplication       : Started LightChatApplication in 3.94 seconds (JVM running for 5.526)
2019-11-03 21:23:23.956  INFO 19636 --- [nio-8080-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
2019-11-03 21:23:23.957  INFO 19636 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
2019-11-03 21:23:23.977  INFO 19636 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 20 ms

```







## 2.2 快速入门解析

### 2.2.2 SpringBoot代码解析

- @SpringBootApplication：标注SpringBoot的启动类，该注解具备多种功能（后面详细剖析）
- SpringApplication.run(MySpringBootApplication.class) 代表运行SpringBoot的启动类，参数为SpringBoot启动类的字节码对象

### 2.2.3 SpringBoot工程热部署

我们在开发中反复修改类、页面等资源，每次修改后都是需要重新启动才生效，这样每次启动都很麻烦，浪费了大量的时间，我们可以在修改代码后不重启就能生效，在 pom.xml 中添加如下配置就可以实现这样的功能，我们称之为热部署。

```xml
<!--热部署配置-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
</dependency>
```

注意：IDEA进行SpringBoot热部署失败原因

出现这种情况，并不是热部署配置问题，其根本原因是因为Intellij IEDA默认情况下不会自动编译，需要对IDEA进行自动编译的设置：

1.file -> settings -> build : complier,打开自动编译

![image-20191106211423262](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191106211423262.png)



2.通过快捷键：shift+ctrl+alt+/，进入registry设置，将app.running选项勾选

![image-20191106211255753](C:\Users\sxt\AppData\Roaming\Typora\typora-user-images\image-20191106211255753.png)



### 2.2.4 使用idea快速创建SpringBoot项目















通过idea快速创建的SpringBoot项目的pom.xml中已经导入了我们选择的web的起步依赖的坐标

```xml

```

可以使用快速入门的方式创建Controller进行访问，此处不再赘述





# 三、SpringBoot的配置文件

## 3.1 SpringBoot配置文件类型

### 3.1.1 SpringBoot配置文件类型和作用

SpringBoot是基于约定的，所以很多配置都有默认值，但如果想使用自己的配置替换默认配置的话，就可以使用application.properties或者application.yml（application.yaml）进行配置。

SpringBoot默认会从Resources目录下加载application.properties或application.yml（application.yaml）文件

其中，application.properties文件是键值对类型的文件，之前一直在使用，所以此处不在对properties文件的格式进行阐述。除了properties文件外，SpringBoot还可以使用yml文件进行配置，下面对yml文件进行讲解。

### 3.1.2 application.yml配置文件

#### 3.1.2.1 yml配置文件简介

YML文件格式是YAML (YAML Aint Markup Language)编写的文件格式，YAML是一种直观的能够被电脑识别的的数据数据序列化格式，并且容易被人类阅读，容易和脚本语言交互的，可以被支持YAML库的不同的编程语言程序导入，比如： C/C++, Ruby, Python, Java, Perl, C#, PHP等。YML文件是以数据为核心的，比传统的xml方式更加简洁。

YML文件的扩展名可以使用.yml或者.yaml。

#### 3.1.2.2 yml配置文件的语法

##### 3.1.2.2.1 配置普通数据

- 语法： key: value

- 示例代码：

- ```yaml
  name: haohao
  ```

- 注意：value之前有一个空格

##### 3.1.2.2.2 配置对象数据

- 语法： 

  	key: 

    		key1: value1

    		key2: value2

    	或者：

    	key: {key1: value1,key2: value2}

- 示例代码：

- ```yaml
  person:
    name: haohao
    age: 31
    addr: beijing

  #或者

  person: {name: haohao,age: 31,addr: beijing}
  ```

- 注意：key1前面的空格个数不限定，在yml语法中，相同缩进代表同一个级别

##### 3.1.2.2.2 配置Map数据 

同上面的对象写法

##### 3.1.2.2.3 配置数组（List、Set）数据

- 语法： 

  	key: 

    		- value1

               		- value2

  或者：

  	key: [value1,value2]

- 示例代码：

- ```yaml
  city:
    - beijing
    - tianjin
    - shanghai
    - chongqing
    
  #或者

  city: [beijing,tianjin,shanghai,chongqing]

  #集合中的元素是对象形式
  student:
    - name: zhangsan
      age: 18
      score: 100
    - name: lisi
      age: 28
      score: 88
    - name: wangwu
      age: 38
      score: 90
  ```

- 注意：value1与之间的 - 之间存在一个空格

### 3.1.3 SpringBoot配置信息的查询

上面提及过，SpringBoot的配置文件，主要的目的就是对配置信息进行修改的，但在配置时的key从哪里去查询呢？我们可以查阅SpringBoot的官方文档

文档URL：https://docs.spring.io/spring-boot/docs/2.0.1.RELEASE/reference/htmlsingle/#common-application-properties

常用的配置摘抄如下：

```properties
# QUARTZ SCHEDULER (QuartzProperties)
spring.quartz.jdbc.initialize-schema=embedded # Database schema initialization mode.
spring.quartz.jdbc.schema=classpath:org/quartz/impl/jdbcjobstore/tables_@@platform@@.sql # Path to the SQL file to use to initialize the database schema.
spring.quartz.job-store-type=memory # Quartz job store type.
spring.quartz.properties.*= # Additional Quartz Scheduler properties.

# ----------------------------------------
# WEB PROPERTIES
# ----------------------------------------

# EMBEDDED SERVER CONFIGURATION (ServerProperties)
server.port=8080 # Server HTTP port.
server.servlet.context-path= # Context path of the application.
server.servlet.path=/ # Path of the main dispatcher servlet.

# HTTP encoding (HttpEncodingProperties)
spring.http.encoding.charset=UTF-8 # Charset of HTTP requests and responses. Added to the "Content-Type" header if not set explicitly.

# JACKSON (JacksonProperties)
spring.jackson.date-format= # Date format string or a fully-qualified date format class name. For instance, `yyyy-MM-dd HH:mm:ss`.

# SPRING MVC (WebMvcProperties)
spring.mvc.servlet.load-on-startup=-1 # Load on startup priority of the dispatcher servlet.
spring.mvc.static-path-pattern=/** # Path pattern used for static resources.
spring.mvc.view.prefix= # Spring MVC view prefix.
spring.mvc.view.suffix= # Spring MVC view suffix.

# DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
spring.datasource.driver-class-name= # Fully qualified name of the JDBC driver. Auto-detected based on the URL by default.
spring.datasource.password= # Login password of the database.
spring.datasource.url= # JDBC URL of the database.
spring.datasource.username= # Login username of the database.

# JEST (Elasticsearch HTTP client) (JestProperties)
spring.elasticsearch.jest.password= # Login password.
spring.elasticsearch.jest.proxy.host= # Proxy host the HTTP client should use.
spring.elasticsearch.jest.proxy.port= # Proxy port the HTTP client should use.
spring.elasticsearch.jest.read-timeout=3s # Read timeout.
spring.elasticsearch.jest.username= # Login username.

```

我们可以通过配置application.poperties 或者 application.yml 来修改SpringBoot的默认配置

例如：

application.properties文件

```properties
server.port=8888
server.servlet.context-path=demo
```

application.yml文件

```yaml
server:
  port: 8888
  servlet:
    context-path: /demo
```



## 3.2 配置文件与配置类的属性映射方式

### 3.2.1 使用注解@Value映射

我们可以通过@Value注解将配置文件中的值映射到一个Spring管理的Bean的字段上

例如：

application.properties配置如下：

```properties
person:
  name: zhangsan
  age: 18
```

或者，application.yml配置如下：

```yaml
person:
  name: zhangsan
  age: 18
```

实体Bean代码如下：

```java

```











### 3.2.2 使用注解@ConfigurationProperties映射

通过注解@ConfigurationProperties(prefix="配置文件中的key的前缀")可以将配置文件中的配置自动与实体进行映射

application.properties配置如下：

```properties
person:
  name: zhangsan
  age: 18
```

或者，application.yml配置如下：

```yaml
person:
  name: zhangsan
  age: 18
```

实体Bean代码如下：

```java

```













# 四、SpringBoot与整合其他技术

## 4.1 SpringBoot整合Mybatis

### 4.1.1 添加Mybatis的起步依赖

```xml
<!--mybatis起步依赖-->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>1.1.1</version>
</dependency>
```

### 4.1.2 添加数据库驱动坐标

```xml
<!-- MySQL连接驱动 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

### 4.1.3 添加数据库连接信息

在application.properties中添加数据量的连接信息

```properties
#DB Configuration:
spring.datasource.driverClassName=com.mysql.jdbc.Driver
spring.datasource.url=jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=utf8
spring.datasource.username=root
spring.datasource.password=root
```

### 4.1.4 创建user表

在test数据库中创建user表

```sql
-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `face_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `face_image_big` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `qrcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cid` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用于消息的推送',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
```

### 4.1.5 创建实体Bean

```java
/**
 * 用户模型
 * @author sxt
 * @create 2019-11-06 20:47
 */
public class Users {

    private String  id;     //用户id
    private String  username; //用户名
    private String  password; //密码
    private String  faceImage; //头像
    private String  faceImageBig;
    private String  nickname;  //昵称
    private String  qrcode;  //用户二维码


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFaceImage() {
        return faceImage;
    }

    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage;
    }

    public String getFaceImageBig() {
        return faceImageBig;
    }

    public void setFaceImageBig(String faceImageBig) {
        this.faceImageBig = faceImageBig;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }
}

```

### 4.1.6 编写Mapper

```java
import com.lightChat.entity.Users;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author sxt
 * @create 2019-11-06 21:25
 */

@Mapper
public interface UsersMapper {

    /**
     * 查询数据库中所有的用户信息
     * @return
     */
    @Select("select * from users")
    List<Users> selectAllUsers();

}
```

注意：@Mapper标记该类是一个mybatis的mapper接口，可以被spring boot自动扫描到spring上下文中

### 4.1.7 配置Mapper映射文件

在src\main\resources\mapper路径下加入UserMapper.xml配置文件"

```xml
<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="">
    
</mapper>
```

### 4.1.8 在application.properties中添加mybatis的信息

```properties
#spring集成Mybatis环境
#pojo别名扫描包
mybatis.type-aliases-package=
#加载Mybatis映射文件
mybatis.mapper-locations=classpath:mapper/*Mapper.xml
```

### 4.1.9 编写测试Controller

```java
import com.lightChat.entity.Users;
import com.lightChat.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author sxt
 * @create 2019-11-06 21:55
 */
@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @RequestMapping("/getAllUsers")
    public List<Users> getAllUsers(){
        List<Users> users = usersService.queryAllUsers();
        return users;
    }
}

```

### 4.1.10 测试









## 4.2 SpringBoot整合Junit

### 4.2.1 添加Junit的起步依赖

```xml
<!--测试的起步依赖-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
```

### 4.2.2 编写测试类

```java

```

其中，

SpringRunner继承自SpringJUnit4ClassRunner，使用哪一个Spring提供的测试测试引擎都可以

```java
public final class SpringRunner extends SpringJUnit4ClassRunner 
```

@SpringBootTest的属性指定的是引导类的字节码对象



### 4.2.3 控制台打印信息











## 4.3 SpringBoot整合Redis（拓展部分）

### 4.3.1 添加redis的起步依赖

```xml
<!-- 配置使用redis启动器 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

### 4.3.2 配置redis的连接信息

```properties
#Redis
spring.redis.host=127.0.0.1
spring.redis.port=6379
```

### 4.3.3 注入RedisTemplate测试redis操作

```java
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootJpaApplication.class)
public class RedisTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    public void test() throws JsonProcessingException {
        //从redis缓存中获得指定的数据
        String userListData = redisTemplate.boundValueOps("user.findAll").get();
        //如果redis中没有数据的话
        if(null==userListData){
            //查询数据库获得数据
            List<User> all = userRepository.findAll();
            //转换成json格式字符串
            ObjectMapper om = new ObjectMapper();
            userListData = om.writeValueAsString(all);
            //将数据存储到redis中，下次在查询直接从redis中获得数据，不用在查询数据库
            redisTemplate.boundValueOps("user.findAll").set(userListData);
            System.out.println("===============从数据库获得数据===============");
        }else{
            System.out.println("===============从redis缓存中获得数据===============");
        }

        System.out.println(userListData);

    }

}
```

# 五、SpringBoot实际项目开发

## 4.1 用户注册

### 4.1.1二维码

Java 操作二维码的开源项目很多，如 SwetakeQRCode、BarCode4j、Zxing 等等。本次项目采用简单易用的 google 公司的 zxing，zxing 使用方便，可以操作条形码或者二维码等。

``` java
<!-- 二维码 -->
		<dependency>
		    <groupId>com.google.zxing</groupId>
		    <artifactId>javase</artifactId>
		    <version>3.3.3</version>
		</dependency>
```





## 4.2 用户登录



## 4.3修改头像



